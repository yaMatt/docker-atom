FROM debian:stable

LABEL maintainer="matt@copperwaite.net"

ARG ATOM_VERSION=1.35.1

RUN apt-get update && \
      apt-get upgrade && \
      apt-get install -y wget gdebi-core && \
      wget https://github.com/atom/atom/releases/download/v$ATOM_VERSION/atom-amd64.deb && \
      gdebi -n atom-amd64.deb

ENTRYPOINT apm
