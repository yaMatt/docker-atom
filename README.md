# Docker Atom
This `Dockerfile` installs Atom inside so it is possible to get at apm

## Arguments
The image is built with the current latest version, which is specified in the `ATOM_VERSION` argument. This [can be over-ridden at build time](https://docs.docker.com/engine/reference/commandline/build/#set-build-time-variables---build-arg) if necessary.

## License
AGPLv3

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
